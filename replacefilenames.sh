find . -name 'EmbeddedContent*' -type f -exec sh -c '
for f; do
    mv "$f" "${f%/*}/OpenaiContent{f##*/EmbeddedContent}"
done' sh {} +
