CKEditor5 OpenAI Content
============

This module provides a way to allow editors to insert styled components into CKEditor without having
to grant the editors permission

- Styling of each component can be
- Editors don't require permission to set classes to HTML
- Developers can create more complex components
- Thanks to Ckeditor5 the elements are immediately upcasted and visible in the editor.

Usage
============

Enable the OpenAI Content text filter:

1. Create a new text filter or edit an existing one and enable CKEditor 5.
2. Drag the 'OpenAI Content' icon to the Ckeditor Toolbar
3. Enable the 'OpenAI Content' filter.

Create one or more 'CkeditorOpenai' plugins.
- Enable ckeditor_openai_examples for a working example.
