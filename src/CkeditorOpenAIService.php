<?php

namespace Drupal\ckeditor_openai;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

class CkeditorOpenAIService extends \Drupal\openai_api\OpenAIService {
  public function callApiCKEditor($method, array $params = []): Response|ResponseInterface {
    $uri = $this->getApiUrl() . $this->getApiUri();
    $params = count($params) ? $params : [];

    try {
      $response = $this->getClient()->request($method, $uri, $params);
    } catch (RequestException $e) {
      $response = new Response(
        $e->getCode(), $e->getResponse()
        ->getHeaders(), $e->getResponse()->getBody()
      );
    }

    return $response;
  }
}
