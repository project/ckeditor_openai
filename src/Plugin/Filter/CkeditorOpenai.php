<?php

namespace Drupal\ckeditor_openai\Plugin\Filter;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Utility\Html;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\ckeditor_openai\CkeditorOpenaiPluginManager;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Provides a text filter that turns < ckeditor-openai > tags into markup.
 *
 * @Filter(
 *   id = "ckeditor_openai",
 *   title = @Translation("OpenAI content"),
 *   description = @Translation("Converts < ckeditor-openai > tags to results."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
 *   weight = 100,
 * )
 *
 * @internal
 */
class CkeditorOpenai extends FilterBase implements ContainerFactoryPluginInterface, TrustedCallbackInterface {

  /**
   * The plugin manager for OpenAI content.
   *
   * @var \Drupal\ckeditor_openai\CkeditorOpenaiPluginManager
   */
  protected $CkeditorOpenaiPluginManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Renderer $renderer, CkeditorOpenaiPluginManager $openai_content_plugin_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->CkeditorOpenaiPluginManager = $openai_content_plugin_manager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('renderer'),
      $container->get('plugin.manager.ckeditor_openai')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $document = Html::load($text);
    $crawler = new Crawler($document);
    $attachments = [];
    $crawler->filter('ckeditor-openai')
      ->each(function (Crawler $crawler) use ($document, &$attachments) {
        /** @var \DOMElement $node */
        $node = $crawler->getNode(0);
        $plugin_config = Json::decode($node->getAttribute('data-plugin-config'));
        $plugin_id = $node->getAttribute('data-plugin-id');

        if(!$plugin_id || !$plugin_config){

        } else {
          try {
            /** @var \Drupal\ckeditor_openai\CkeditorOpenaiInterface $instance */
            $instance = $this->CkeditorOpenaiPluginManager->createInstance($plugin_id, $plugin_config);
            $attachments = BubbleableMetadata::mergeAttachments($attachments, $instance->getAttachments());
            $replacement = $instance->build();
            $render = $this->renderer->renderPlain($replacement);
          } catch (Exception $e){
            $render = (new TranslatableMarkup('Something went wrong.'));
          }
          libxml_use_internal_errors(TRUE);
          $new = Html::load($render);
          libxml_clear_errors();
          $new_node = $document->importNode($new->documentElement, TRUE);
          libxml_use_internal_errors(FALSE);
          $node->parentNode->replaceChild($new_node, $node);
        }
      });
    $result = new FilterProcessResult(Html::serialize($document));
    $result->addAttachments($attachments);
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return [];
  }

}
