<?php

declare(strict_types = 1);

namespace Drupal\ckeditor_openai\Plugin\CKEditor5Plugin;

use Drupal\ckeditor5\Plugin\CKEditor5PluginDefault;
use Drupal\Core\Url;
use Drupal\editor\EditorInterface;

/**
 * Plugin class to add dialog url for OpenAI content.
 */
class CkeditorOpenai extends CKEditor5PluginDefault {

  /**
   * {@inheritdoc}
   */
  public function getDynamicPluginConfig(array $static_plugin_config, EditorInterface $editor): array {
    $openai_content_dialog_url = Url::fromRoute('ckeditor_openai.dialog')
      ->toString(TRUE)
      ->getGeneratedUrl();
    $static_plugin_config['ckeditorOpenai']['dialogURL'] = $openai_content_dialog_url;
    $openai_content_preview_url = Url::fromRoute('ckeditor_openai.preview', [
      'editor' => $editor->id(),
    ])
      ->toString(TRUE)
      ->getGeneratedUrl();
    $static_plugin_config['ckeditorOpenai']['previewURL'] = $openai_content_preview_url;
    return $static_plugin_config;
  }

}
