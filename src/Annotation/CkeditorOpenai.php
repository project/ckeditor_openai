<?php

namespace Drupal\ckeditor_openai\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines openai_content annotation object.
 *
 * @Annotation
 */
class CkeditorOpenai extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
