import {Plugin} from 'ckeditor5/src/core';
import {toWidget, Widget} from 'ckeditor5/src/widget';
import CkeditorOpenaiCommand from './openaicontentcommand';

/**
 * OpenAI content editing functionality.
 */
export default class CkeditorOpenaiEditing extends Plugin {

  /**
   * @inheritdoc
   */
  static get requires() {
    return [Widget];
  }

  /**
   * @inheritdoc
   */
  init() {
    this.attrs = {
      ckeditorOpenaiPluginConfig: 'data-plugin-config',
      ckeditorOpenaiPluginId: 'data-plugin-id',
    };
    const options = this.editor.config.get('ckeditorOpenai');
    if (!options) {
      return;
    }
    const {previewURL, themeError} = options;
    this.previewUrl = previewURL;
    this.themeError =
      themeError ||
      `
      <p>${Drupal.t(
        'An error occurred while trying to preview the OpenAI content. Please save your work and reload this page.',
      )}<p>
    `;

    this._defineSchema();
    this._defineConverters();

    this.editor.commands.add(
      'ckeditorOpenai',
      new CkeditorOpenaiCommand(this.editor),
    );
  }

  /**
   * Fetches the preview.
   */
  async _fetchPreview(modelElement) {
    const query = {
      plugin_id: modelElement.getAttribute('ckeditorOpenaiPluginId'),
      plugin_config: modelElement.getAttribute('ckeditorOpenaiPluginConfig'),
    };
    const response = await fetch(
      `${this.previewUrl}?${new URLSearchParams(query)}`
    );
    if (response.ok) {
      return await response.text();
    }

    return this.themeError;
  }

  /**
   * Registers ckeditorOpenai as a block element in the DOM converter.
   */
  _defineSchema() {
    const schema = this.editor.model.schema;
    schema.register('ckeditorOpenai', {
      allowWhere: '$block',
      isObject: true,
      isContent: true,
      isBlock: true,
      allowAttributes: Object.keys(this.attrs),
    });
    this.editor.editing.view.domConverter.blockElements.push('ckeditor-openai');
  }

  /**
   * Defines handling of drupal media element in the content lifecycle.
   *
   * @private
   */
  _defineConverters() {
    const conversion = this.editor.conversion;

    conversion
      .for('upcast')
      .elementToElement({
        view: {
          name: 'ckeditor-openai',
        },
        model: 'ckeditorOpenai',
      });

    conversion
      .for('dataDowncast')
      .elementToElement({
        model: 'ckeditorOpenai',
        view: {
          name: 'ckeditor-openai',
        },
      });
    conversion
      .for('editingDowncast')
      .elementToElement({
        model: 'ckeditorOpenai',
        view: (modelElement, {writer}) => {
          const container = writer.createContainerElement('figure');
          return toWidget(container, writer, {
            label: Drupal.t('OpenAI content'),
          });

        },
      })
      .add((dispatcher) => {
        const converter = (event, data, conversionApi) => {
          const viewWriter = conversionApi.writer;
          const modelElement = data.item;
          const container = conversionApi.mapper.toViewElement(data.item);
          const ckeditorOpenai = viewWriter.createRawElement('div', {
            'data-ckeditor-openai-preview': 'loading',
            'class': 'ckeditor-openai-preview'
          });
          viewWriter.insert(viewWriter.createPositionAt(container, 0), ckeditorOpenai);
          this._fetchPreview(modelElement).then((preview) => {
            if (!ckeditorOpenai) {
              return;
            }
            this.editor.editing.view.change((writer) => {
              const ckeditorOpenaiPreview = writer.createRawElement(
                'div',
                {'class': 'ckeditor-openai-preview', 'data-ckeditor-openai-preview': 'ready'},
                (domElement) => {
                  domElement.innerHTML = preview;
                },
              );
              writer.insert(writer.createPositionBefore(ckeditorOpenai), ckeditorOpenaiPreview);
              writer.remove(ckeditorOpenai);
            });
          });
        };
        dispatcher.on('attribute:ckeditorOpenaiPluginId:ckeditorOpenai', converter);
        return dispatcher;
      });

    Object.keys(this.attrs).forEach((modelKey) => {
      const attributeMapping = {
        model: {
          key: modelKey,
          name: 'ckeditorOpenai',
        },
        view: {
          name: 'ckeditor-openai',
          key: this.attrs[modelKey],
        },
      };
      conversion.for('dataDowncast').attributeToAttribute(attributeMapping);
      conversion.for('upcast').attributeToAttribute(attributeMapping);
    });
  }

  /**
   * @inheritdoc
   */
  static get pluginName() {
    return 'ckeditorOpenaiEditing';
  }
}
