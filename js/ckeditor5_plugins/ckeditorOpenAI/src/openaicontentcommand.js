import { Command } from 'ckeditor5/src/core';

/**
 * Creates OpenAI content
 */
function createCkeditorOpenai(writer, attributes) {
  return writer.createElement('ckeditorOpenai', attributes);
}

/**
 * Command for inserting <ckeditor-openai> tag into ckeditor.
 */
export default class CkeditorOpenaiCommand extends Command {
  execute(attributes) {
    const ckeditorOpenaiEditing = this.editor.plugins.get('ckeditorOpenaiEditing');

    // Create object that contains supported data-attributes in view data by
    // flipping `DrupalMediaEditing.attrs` object (i.e. keys from object become
    // values and values from object become keys).
    const dataAttributeMapping = Object.entries(ckeditorOpenaiEditing.attrs).reduce(
      (result, [key, value]) => {
        result[value] = key;
        return result;
      },
      {},
    );

    // \Drupal\media\Form\EditorMediaDialog returns data in keyed by
    // data-attributes used in view data. This converts data-attribute keys to
    // keys used in model.
    const modelAttributes = Object.keys(attributes).reduce(
      (result, attribute) => {
        if (dataAttributeMapping[attribute]) {
          result[dataAttributeMapping[attribute]] = attributes[attribute];
        }
        return result;
      },
      {},
    );

    this.editor.model.change((writer) => {
      this.editor.model.insertContent(
        createCkeditorOpenai(writer, modelAttributes),
      );
    });
  }

  refresh() {
    const model = this.editor.model;
    const selection = model.document.selection;
    const allowedIn = model.schema.findAllowedParent(
      selection.getFirstPosition(),
      'ckeditorOpenai',
    );
    this.isEnabled = allowedIn !== null;
  }
}
