import { Plugin } from 'ckeditor5/src/core';
import CkeditorOpenaiEditing from './openaicontentediting';
import CkeditorOpenaiUI from './openaicontentui';

/**
 * Main entry point to the OpenAI content js plugin.
 */
export default class CkeditorOpenai extends Plugin {

  /**
   * @inheritdoc
   */
  static get requires() {
    return [
      CkeditorOpenaiEditing,
      CkeditorOpenaiUI
    ];
  }

  /**
   * @inheritdoc
   */
  static get pluginName() {
    return 'ckeditorOpenai';
  }
}
