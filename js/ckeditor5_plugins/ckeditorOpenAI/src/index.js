import CkeditorOpenai from "./openaicontent";

/**
 * @private
 */
export default {
  CkeditorOpenai: CkeditorOpenai
};
