<?php

namespace Drupal\ckeditor_openai_dall_e\Plugin\CkeditorOpenai;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\ckeditor_openai\CkeditorOpenaiInterface;
use Drupal\ckeditor_openai\CkeditorOpenaiPluginBase;

/**
 * OpenAI Plugin.
 *
 * @CkeditorOpenai(
 *   id = "dalle",
 *   label = @Translation("DALL-E"),
 *   description = @Translation("Provides a DALL-E prompt to receive images"),
 * )
 */
class Dalle extends CkeditorOpenaiPluginBase implements CkeditorOpenaiInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'input_text' => NULL,
      'returned_image' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    return [
      '#theme' => 'ckeditor_openai_dalle',
      '#input_text' => $this->configuration['input_text'],
      '#returned_image' => $this->configuration['returned_image'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['input_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Input Text'),
      '#default_value' => $this->configuration['input_text'],
      '#required' => TRUE,
    ];
    $form['returned_image'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Returned Image'),
      '#default_value' => $this->configuration['returned_image'],
      '#access' => FALSE
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    if($form_state->getCompleteFormState()->getTriggeringElement()['#type'] === 'select'){
      return;
    }
    /**
     * @var $openAiService \Drupal\openai_api\OpenAIService
     */
    $openAiService = \Drupal::service('openai_api.openai.service');
    $openAiService->setApiUri('/v1/images/generations');
    $res = $openAiService->getImageUrl($form_state->getValues()['input_text'],'1024x1024');
    $result = json_decode($res->getBody()->getContents());
    $form_state->setValue('returned_image',$result->data[0]->url);
  }



}
