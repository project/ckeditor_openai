<?php

namespace Drupal\ckeditor_openai_seo\Plugin\CkeditorOpenai;

use Drupal\ckeditor_openai\CkeditorOpenAIService;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\ckeditor_openai\CkeditorOpenaiInterface;
use Drupal\ckeditor_openai\CkeditorOpenaiPluginBase;

/**
 * OpenAI Plugin.
 *
 * @CkeditorOpenai(
 *   id = "seo_openai",
 *   label = @Translation("SEO text-generation"),
 *   description = @Translation("Provides a SEO Plugin"),
 * )
 */
class Seo extends CkeditorOpenaiPluginBase implements CkeditorOpenaiInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'input_text' => NULL,
      'returned_text' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    return [
      '#theme' => 'ckeditor_openai_seo',
      '#input_text' => $this->configuration['input_text'],
      '#returned_text' => $this->configuration['returned_text'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['input_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Input Words'),
      '#default_value' => $this->configuration['input_text'],
      '#required' => TRUE,
      '#description' => 'Insert words separated by comma (e.g. apple,kiwi) to generate a SEO-compliant text compatible'
    ];
    $form['returned_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Returned Text'),
      '#default_value' => $this->configuration['returned_text'],
      '#access' => FALSE
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    if($form_state->getCompleteFormState()->getTriggeringElement()['#type'] === 'select'){
      return;
    }
    $words = $form_state->getValues()['input_text'];
    $baseText = "Write a SEO-compliant article with the following words: $words";
    /**
     * @var $openAiService \Drupal\openai_api\OpenAIService
     */
    $openAiService = \Drupal::service('openai_api.openai.service');
    $openAiService->setApiUri('/v1/completions');
    $res = $openAiService->getText('text-davinci-003',$baseText,1500,0);
    $result = json_decode($res->getBody()->getContents());
    $form_state->setValue('returned_text',$result->choices[0]->text);
  }



}
