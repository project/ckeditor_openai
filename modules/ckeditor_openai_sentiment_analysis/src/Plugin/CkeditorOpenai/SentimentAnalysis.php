<?php

namespace Drupal\ckeditor_openai_sentiment_analysis\Plugin\CkeditorOpenai;

use Drupal\ckeditor_openai\CkeditorOpenAIService;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\ckeditor_openai\CkeditorOpenaiInterface;
use Drupal\ckeditor_openai\CkeditorOpenaiPluginBase;

/**
 * OpenAI Plugin.
 *
 * @CkeditorOpenai(
 *   id = "sentiment_analysis",
 *   label = @Translation("Sentiment Analysis"),
 *   description = @Translation("Provides a OpenAI Sentiment Analysis Plugin"),
 * )
 */
class SentimentAnalysis extends CkeditorOpenaiPluginBase implements CkeditorOpenaiInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'input_text' => NULL,
      'returned_text' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    return [
      '#theme' => 'ckeditor_openai_sentiment_analysis',
      '#input_text' => $this->configuration['input_text'],
      '#returned_text' => $this->configuration['returned_text'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['input_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Input Text'),
      '#default_value' => $this->configuration['input_text'],
      '#required' => TRUE,
    ];
    $form['returned_text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Returned Text'),
      '#default_value' => $this->configuration['returned_text'],
      '#access' => FALSE
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    if($form_state->getCompleteFormState()->getTriggeringElement()['#type'] === 'select'){
      return;
    }
    /**
     * @var $openAiService \Drupal\openai_api\OpenAIService
     */
    $openAiService = \Drupal::service('openai_api.openai.service');
    $token = $openAiService->getApiToken();
    $text = $form_state->getValues()['input_text'];
    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://api.openai.com/v1/moderations',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'POST',
      CURLOPT_POSTFIELDS =>"{
  \"input\": \"$text\"
}",
      CURLOPT_HTTPHEADER => array(
        'Content-Type: application/json',
        "Authorization: Bearer $token"
      ),
    ));

    $result = curl_exec($curl);
    $res = json_decode($result);
    $categories = (array) $res->results[0]->categories;
    curl_close($curl);
    $error = '';
    $policies = [];
    foreach($categories as $name => $value){
      if($value){
        $policies[] = $name;
      }
    }
    if(!empty($policies)){
      $violatedPoliciesMessage = implode(',',$policies);
      $form_state->setErrorByName('input_text',$this->t("Your content violates the following policies: $violatedPoliciesMessage"));
    }
    $form_state->setValue('returned_text',$form_state->getValues()['input_text']);
  }



}
